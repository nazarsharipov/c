// Вывести числовую пирамидку на total строк. Порядок строк – обратный.
// В каждой строке числа идут от единицы до номера строки через пробел.

// Пример ввода
// 3
// Пример вывода
// 1 2 3
// 1 2
// 1

#include <stdio.h>

int main() {
    int size;
    
    scanf("%d", &size);
    for ( ; size >= 1; size-- ) {
        for ( int col = 1; col < size; col++ ) {
            printf("%d ", col);
        }
        printf("%d\n", size);
    }
    return 0;
}
