// В файле task.in дано искомое число needle, а также последовательность чисел размером не более 100 элементов.
// Найти needle в массиве и вывести в файл task.out его индекс либо -1, если число не найдено.

// Пример ввода
// 40
// 10 20 30 40 50 60 70 80 90 100
// Пример вывода
// 3

#include <stdio.h>

int arraySearch(int array[], int size, int needle) {
    for ( int i = 0; i < size; i++ ) {
        if ( needle == array[i] ) {
            return i;
        }
    }
    return -1;
}

int main() {
    FILE *in = fopen("task.in", "r");
    FILE *out = fopen("task.out", "w");
    int res, needle;
    int size = 100;
    int array[size];
    int count = 1;
    
    fscanf(in, "%d", &needle);
    for ( int i = 0; fscanf(in, "%d", &array[i]) > 0 && count < size; i++ ) {
        count += 1;
    }
    size = count;
    res = arraySearch(array, size, needle);
    fprintf(out, "%d\n", res);
    fclose(in);
    fclose(out);
    
    return 0;
}

