// Вывести число Фибоначчи с заданным целым номером, по модулю не превышающим 46.
// Пример ввода
// 8
// Пример вывода
// 21

#include <stdio.h>

int main() {
    int n, tmp;
    int index0 = 0;
    int index1 = 1;
    int index2 = 1;
    int index_1 = 1;
    
    scanf("%d", &n);
    if ( n == 0 ) {
        printf("0\n");
    }
    if ( n == 1 || n == 2 || n == -1 ) {
        printf("1\n");
    }
    if ( n > 2 && n <= 46 ) {
        for ( int i = 3; i <= n; i++ ) {
            tmp = index2;
            index2 = index1 + index2;
            index1 = tmp;
        }
        printf("%d\n", index2);
    } else if ( n < -1 && n >= -46 ) {
        for ( int i = -2; i >= n; i-- ) {
            tmp = index_1;
            index_1 = index0 - index_1;
            index0 = tmp;
        }
        printf("%d\n", index_1);
    }
    return 0;
}
