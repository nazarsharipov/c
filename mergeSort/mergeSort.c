#include <stdio.h>

void merge(int array[], int lo, int mid, int hi) {
    int len = hi - lo;
    int tmp[len];
    int index = 0;
    int loIndex = lo;
    int hiIndex = mid;
    
    for ( ; loIndex < mid && hiIndex < hi; index++ ) {
        if ( array[loIndex] <= array[hiIndex] ) {
            tmp[index] = array[loIndex];
            loIndex += 1;
        } else {
            tmp[index] = array[hiIndex];
            hiIndex += 1;
        }
    }
    
    for ( int i = loIndex; i < mid; i++, index++ ) {
        tmp[index] = array[i];
    }
    for ( int i = hiIndex; i < hi; i++, index++ ) {
        tmp[index] = array[i];
    }
    for ( int i = 0, j = lo; j < hi; i++, j++ ) {
        array[j] = tmp[i];
    }
}

void mergeSort(int array[], int lo, int hi) {
    int mid = lo / 2 + hi / 2;
    
    if ( lo < mid ) {
        mergeSort(array, lo, mid);
        mergeSort(array, mid, hi);
        merge(array, lo, mid, hi);
    }
    merge(array, lo, mid, hi);
}

int getLen(FILE *in) {
    int len;
    
    fscanf(in, "%d", &len);
    
    return len;
}

void arrayScan(FILE *in, int array[], int limit) {
    int counter = 0;
    
    for ( ; counter < limit && fscanf(in, "%d", &array[counter]) == 1; counter++ );
}

void arrayPrint(FILE *out, int array[], int size) {
    int last = size - 1;
    
    for ( int i = 0; i < last; i++ ) {
        fprintf(out, "%d ", array[i]);
    }
    fprintf(out, "%d\n", array[last]);
}

int main() {
    FILE *in = fopen("task.in", "r");
    FILE *out = fopen("task.out", "w");
    int len = getLen(in);
    int array[len];
    int lo = 0;
    int hi = len;
    
    arrayScan(in, array, len);
    
    mergeSort(array, lo, hi);
    
    arrayPrint(out, array, len);
    
    fclose(in);
    fclose(out);
    
    return 0;
}
