void bubbleSort(int array[], int size) {
    int temp = size;
    
    for ( int i = temp; i > 0; i-- ) {
        for ( int j = 1; j < i; j++ ) {
            if ( array[j-1] > array[j] ) {
                int change = array[j-1];
                
                array[j-1] = array[j];
                array[j] = change;
            }
        }
    }
}
