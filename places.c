// Считать с клавиатуры целое число.
// Вывести на экран количество знакомест, нужных для вывода этого числа.

// Пример ввода
// 123
// Пример вывода
// 3

#include <stdio.h>

int main() {
    int number;
    int count = 1;
    
    scanf("%d", &number);
    if ( number < 0 ) {
        number *= -1;
        count += 1;
    }
    for ( int i = number; i >= 10; ) {
        i = i / 10;
        count += 1;
    }
    printf("%d\n", count);
    return 0;
}
