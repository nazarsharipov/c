// Вывести числовой квадрат заданного размера.
// Выведенные числа начинаются с единицы и постоянно увеличиваются.
// В каждой строке числа разделены пробелами.
// Размер считать с клавиатуры.

// Пример ввода
// 2
// Пример вывода
// 1 2
// 3 4

#include <stdio.h>

int main() {
    int size;
    int count = 0;
    
    scanf("%d", &size);
    for ( int row = 1; row <= size; row++ ) {
        count++;
        for ( int col = 1; col < size; col++ ) {
            printf("%d ", count);
            count++;
        }
        printf("%d\n", count);
    }
    return 0;
}
