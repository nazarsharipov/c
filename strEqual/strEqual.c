// В файле task.in заданы две строки длиной не более 100 символов каждая.
// Если строки равны, вывести в task.out слово "yes" - иначе вывести "no".

// Пример ввода
// hello
// hello
// Пример вывода
// yes

#include <stdio.h>

int strEqual(char str1[], char str2[]) {
    for ( int i = 0; str1[i] != '\0' || str2[i] != '\0'; i++ ) {
        if ( str1[i] != str2[i] ) {
            return 0;
        }
    }
    return 1;
}

int main() {
    FILE *in = fopen("task.in", "r");
    FILE *out = fopen("task.out", "w");
    int size = 101;
    int res = 0;
    char str1[size];
    char str2[size];
    
    fscanf(in, "%100s %100s", str1, str2);
    res = strEqual(str1, str2);
    if ( res == 1 ) {
        fprintf(out, "yes\n");
    } else {
        fprintf(out, "no\n");
    }
    return 0;
}

