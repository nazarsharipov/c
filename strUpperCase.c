void strUpperCase(char str[]) {
    char diff = 'a' - 'A';
    
    for ( ; *str != 0; str++ ) {
        if ( *str >= 'a' && *str <= 'z' ) {
            *str -= diff;
        }
    }
}

