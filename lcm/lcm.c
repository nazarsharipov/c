// В файле task.in даны два целых положительных числа.
// Записать в файл task.out наименьшее общее кратное этих чисел.
// Пример ввода
// 3 5
// Пример вывода
// 15

#include <stdio.h>

int gcd(unsigned int a, unsigned int b) {
    if ( b == 0 ) {
        return a;
    }
    return gcd(b, a%b);
}

int main() {
    FILE *in = fopen("task.in", "r");
    FILE *out = fopen("task.out", "w");
    unsigned int x, y, res, lcm;
    
    fscanf(in, "%d %d", &x, &y);
    fclose(in);
    res = gcd(x, y);
    lcm = x / res * y;
    fprintf(out, "%d\n", lcm);
    fclose(out);
    return 0;
}
