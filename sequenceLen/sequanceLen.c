// Считать из файла task.in последовательность чисел размером не более 100 элементов.
// Посчитать длину последовательности и вывести ее в task.out.

// Пример ввода
// 10 20 30 40 50 60 70 80 90 100
// Пример вывода
// 10

#include <stdio.h>

int main() {
    FILE *in = fopen("task.in", "r");
    FILE *out = fopen("task.out", "w");
    int len = 0;
    
    for ( int i; fscanf(in, "%d", &i) > 0 && len < 100; ) {
        len += 1;
    }
    fprintf(out, "%d\n", len);
    fclose(in);
    fclose(out);
    return 0;
}
