// В файле task.in дано число len и массив размером len элементов.
// Отсортировать элементы в порядке неубывания.
// Использовать алгоритм quickSort.
// Результат записать в task.out
#include <stdio.h>

void swap(int *a, int *b) {
    int tmp = *a;
    
    *a = *b;
    *b = tmp;
}

int partition(int array[], int start, int end) {
    int avg = (start + end) / 2;
    int tail = start;
    
    swap(&array[avg], &array[end]);
    for ( ; array[tail] < array[end]; tail++ );
    for ( int i = tail + 1; i < end; i++ ) {
        if ( array[i] < array[end] ) {
            swap(&array[i], &array[tail]);
            tail += 1;
        }
    }
    swap(&array[tail], &array[end]);
    
    return tail;
}

void quickSort(int array[], int lo, int hi) {
    if ( lo < hi ) {
        int pivot = partition(array, lo, hi);
        
        quickSort(array, lo, pivot-1);
        quickSort(array, pivot+1, hi);
    }
}

int sizeArray(FILE *in) {
    int len;
    
    fscanf(in, "%d", &len);
    return len;
}

int main() {
    FILE *in = fopen("task.in", "r");
    FILE *out = fopen("task.out", "w");
    int len = sizeArray(in);
    int array[len];
    int tmp = len - 1;
    
    for ( int i = 0; i < len; i++ ) {
        fscanf(in, "%d", &array[i]);
    }
    quickSort(array, 0, len-1);
    for ( int i = 0; i < tmp; i++ ) {
        fprintf(out, "%d ", array[i]);
    }
    fprintf(out, "%d\n", array[tmp]);
    fclose(in);
    fclose(out);
    return 0;
}
