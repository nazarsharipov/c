void strLowerCase(char str[]) {
    for ( ; *str != 0; str++ ) {
        if ( *str >= 'A' && *str <= 'Z' ) {
            *str -= 'A' - 'a';
        }
    }
}

