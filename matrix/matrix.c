#include <stdio.h>
#define SIZE 5

// Поворот матрицы на 90 градусов по часовой стрелке.
void matrixRotate90(int target[SIZE][SIZE], int source[SIZE][SIZE], int size) {
    int temp = size - 1;
    
    for ( int row = 0; row < size; temp--, row++ ) {
        for ( int col = 0; col < size; col++ ) {
            target[temp][col] = source[col][row];
            target[temp][temp] = source[temp][row];
            target[row][temp] = source[temp][temp];
            target[row][col] = source[col][temp];
        }
    }
}

// Поворот матрицы на 180 градусов по часовой стрелке.
void matrixRotate180(int target[SIZE][SIZE], int source[SIZE][SIZE], int size) {
    int temp = size - 1;
    
    for ( int row = 0; row < size; temp--, row++ ) {
        for ( int col = 0, last = size - 1; col < size || last >= 0; last--, col++ ) {
            target[row][col] = source[temp][last];
            target[temp][last] = source[row][col];
            target[row][temp] = source[temp][row];
            target[temp][row] = source[row][temp];
        }
    }
}

// Поворот матрицы на 270 градусов по часовой стрелке.
void matrixRotate270(int target[SIZE][SIZE], int source[SIZE][SIZE], int size) {
    int temp = size - 1;
    
    for ( int row = 0; row < size; temp--, row++ ) {
        for ( int col = 0; col < size; col++ ) {
            target[col][row] = source[temp][col];
            target[temp][row] = source[temp][temp];
            target[temp][temp] = source[row][temp];
            target[col][temp] = source[row][col];
        }
    }
}

int main() {
    FILE *in = fopen("task.in", "r");
    FILE *out = fopen("task.out", "w");
    int source[SIZE][SIZE], target[SIZE][SIZE];
    int temp = SIZE - 1;

    for ( int row = 0; row < SIZE; row++ ) {
        for ( int col = 0; col < SIZE && fscanf(in, "%d", &source[col][row]) > 0; col++ );
    }
    matrixRotate90(target, source, SIZE);
    
    
    for ( int row = 0; row < SIZE; row++ ) {
        for ( int col = 0; col < temp; col++ ) {
            fprintf(out, "%d ", target[col][row]);
        }
        fprintf(out, "%d\n", target[temp][row]);
    }
    return 0;
}