// Вывести N-ное по счёту простое число.
// N считать с клавиатуры.
// Оптимизировать работу программы, насколько это возможно.
// В случае неопределенности ответа вывести -1.

// Пример ввода
// 3
// Пример вывода
// 5

#include <stdio.h>

int isPrime(int n) {
    for ( int i = 2; i <= n; i += 1 ) {
        if ( n % i == 0 ) {
            if ( n == i ) {
                return 1;
            } else {
                return 0;
            }
        }
    }
    return 0;
}

int main() {
    int x, res, index;
    int count = 0;
    
    scanf("%d", &index);
    
    if ( index < 1 ) {
        printf("-1\n");
    } else {
        for ( x = 1; x < 1000; x++ ) {
            res = isPrime(x);
            if ( res == 1 ) {
                count += 1;
                if ( count == index ) {
                    printf("%d\n", x);
                }
            }
        }
    }
    return 0;
}
