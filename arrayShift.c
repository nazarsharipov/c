// Написать функцию:
// void arrayShift(int array[], int size, int shift)
// Реализовать циклический сдвиг массива на shift элементов вправо. Максимизировать производительность при оптимальном использовании памяти.

#include <stdio.h>

void arrayShift(int array[], int size, int shift) {
    int flag = 1;
    int halfSize = size / 2;
    int temp[halfSize];
    
    if ( shift < 0 ) {
        shift *= -1;
        flag = 0;
    }
    if ( shift >= size ) {
        shift %= size;
    }
    if ( shift > halfSize && shift < size ) {
        shift = size - shift;
        flag = !flag;
    }
    if ( flag ) {
        for ( int i = 0, j = size - shift; i < shift; i++, j++ ) {
            temp[i] = array[j];
        }
        for ( int i = size - 1, j = i - shift; i >= 0; i--, j-- ) {
            array[i] = array[j];
        }
        for ( int i = 0; i < shift; i++ ) {
            array[i] = temp[i];
        }
    } else {
        for ( int i = 0; i < shift; i++ ) {
            temp[i] = array[i];
        }
        for ( int i = shift, j = i - shift; i < size; i++, j++ ) {
            array[j] = array[i];
        }
        for ( int i = size - shift, j = 0; j < shift; i++, j++ ) {
            array[i] = temp[j];
        }
    }
}

int main() {
    int array[10];

    for ( int i = 0; i < 10; i++ ) {
        array[i] = i;
    }

    for (int i = 0; i < 10; i++) {
        printf("%d ", array[i]);
    }
    printf("\n");

    arrayShift(array, 10, 8);

    for (int i = 0; i < 10; i++) {
        printf("%d ", array[i]);
    }
    return 0;
}
